#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <math.h>
#include <time.h>
#include <string.h>

#include "grass.h"

#define N 3

int main(int argc, char** argv) {
    
    if (argc < 2) { 
        printf("usage: ./degree state \n \n ");
        return(-1);
    }

    uint64_t state = strtoull(argv[1], NULL, 10);

    if (state >= bit_pow4((uint64_t) N)*factorial((uint64_t) N)) {
        printf("error: state %ld too large, max for N=%d is %ld \n", state, N, bit_pow4(N)*factorial(N));
        return(-1);
    }
    
    int* w_test = (int*) calloc(2*N, sizeof(int));

	num_to_board(state, w_test, N);
	
	printf("w_test : [");
	for (int i = 0; i < 2*N; i++) {
		printf("%d ", w_test[i]);
	}
	printf("] \n");

	print_comp_board(b_test, w_test, N);

	int deg = calc_deg(b_test, w_test, N);
	printf("Degree for state %lu is %d \n \n", state, deg);

    return 0;

}
