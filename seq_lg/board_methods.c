#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <limits.h>

#include "grass.h"

int arg_in_arr(int val, int* arr, int len) {
    
    int i;
    for(i = 0; i<len; i++) {
        if(*(arr + i) == val) {
            return i;
        }
    }
    return -1;
}

uint64_t factorial(uint64_t n) {
    uint64_t res = n;
    for (uint64_t i = 2; i < n; i++) {
        res = res * i;
    }
    return res;
}

uint64_t bit_pow4(uint64_t n) {
    uint64_t res = 4;
    for (uint64_t i = 1; i < n; i++) {
        res = res * 4;
    }
    if (res == UINT_MAX) {
        printf("bit_pow4 error: power %ld too large, hit UINT_MAX", n);
        return 0;
    }
    return res;
}

// Takes in a set of 2*n black checkers (x,y positions) and n white checkers (in a 2*n array)
// and prints the resulting board visually.
void print_comp_board(int* bb_col, int* wb_col, int n) {
    
    int i, j;
    for (i=0; i<(2*n); i++) {
        printf("\n");

        int bpos = bb_col[i];
        int wpos = wb_col[i];

        /* -1 used as marker for no wc on that row */
        if ((bpos < 0 || bpos >= (2*n)) || (wpos < -1 || (wpos >= (2*n) && wpos != -1))) {
            printf("Col %d has invalid positions, bc col %d, wc col %d \n \n", i, bpos, wpos);
            printf("n: %d \n ", n);
            return;
        }

        for (j=0; j<2*n; j++) {
            
            if (bpos == j) {
                if (wpos == j) {
                    printf(" B ");
                } else {
                    printf(" X ");
                }
            }
            else if(wpos == j) {
                printf(" O ");
            }
            else {
                printf(" . ");
            }
        }   
    }
    printf("\n \n");
}

/* Places checkers in columns (index is row, value is col) 
    for a 2n x 2n board (LG case) 
    
    IN: 
        state - number less than (4^n)*(n!) representing wc pos
        c_col - array of size 2*n where checker pos will be output
                note: -1 used to represent no white checker on that row
        n - corresponds to LG(n,2n) - boards are of size 2n x 2n */ 
void num_to_board(uint64_t state, int* c_col, int n) {

    int i, j;
    int* j_placement = calloc(2*n,sizeof(unsigned int));
    long long unsigned int fac = (bit_pow4(n))*factorial(n);

    if (state >= fac) {
        //printf("Improper state %d is greater than %d! = %d \n \n", state, n, fac);
        return;
    }

    for (i = n-1; i >= 0; i--) {
        fac = fac / (4*(i+1));
        //printf("Step %d fac %d, current state %d \n", n-1-i, fac, state);
        j_placement[n-1-i] = state / fac;
        state = state % fac;
    }

    int* placed = calloc(2*n, sizeof(int));
    int count;
    for (i = 0; i < n; i++) {
        count = j_placement[i];
        int sec = 0;

        /* In this case, we want to note to place the checker
          on the symm row and treat count as a count mod a full row */
        if (count >= 2*(n-i)) {
            sec = 1;
            count -= 2*(n-i);
        }

        j = 0;
        while (count >= 0 && j < 2*n) {
            if(count == 0 && placed[j] == 0) {
                count--;
                placed[j] = 1;
                placed[2*n-1-j] = 1;
                if (sec) {
                    c_col[2*n-1-i] = j; //(2*n -1) - j;
                    c_col[i] = -1;
                } else {
                    c_col[i] = j;
                    c_col[2*n-1-i] = -1;
                }
            } else if (placed[j] == 0) {
                count--;
            }
            j++;
        }
    }
    free(placed);

    free(j_placement);
    

}