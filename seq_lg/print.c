#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>

#include "grass.h"

int main(int argc, char** argv) {
    
    if (argc < 2) { 
        printf("usage: ./print state \n \n ");
        return(-1);
    }

	uint64_t state = strtoull(argv[1], NULL, 10);
    
    int* w_test = (int*) calloc(2*N, sizeof(int));
    num_to_board(state, w_test, N);

    printf("w_test: [");
    for (int i = 0; i < 2*N; i++) {
        printf("%d ", w_test[i]);
    }
    printf("] \n");
    
    print_comp_board(b_test, w_test, N);
    printf("NW CHECK: %d \n", check_NW(b_test, w_test, N));

    return 0;

}
