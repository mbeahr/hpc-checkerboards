#include <stdlib.h>
#include <stdio.h>
#include "grass.h"

bool check_NW(int* bb_col, int* wb_col, int n) {

    // Checks for west
    for (int i=0; i<=2*n; i++) {
        if(bb_col[i] > wb_col[i] && wb_col[i] != -1) {
            return 0;
        }
    }

    // Checks for north
    int checked = 0;
    while (checked < 2*n) {
        int j = 0;
        while (j < 2*n) {
            if (bb_col[j] == checked) {
                j = 2*n;
            } else if(wb_col[j] == checked) {  
                return 0;
            }
            j++;
        }
        checked++;
    }

    return 1;
}

int calc_deg(int* bb_col, int* wb_col, int n) {
    
    int degree = 0;

    // Degree accrued from black checkers
    // First iterate over white checkers across rows
    for(int i = 0; i < 2*n; i++) {
        if (wb_col[i] != -1) {
            // Iterate over previous rows' black checkers
            for (int j = 0; j <= i; j++) {
                if(bb_col[j] <= wb_col[i]) {
                    degree++;
                    //printf("wc: %d has NW bc %d \n", wb_col[i], bb_col[j]);
                }
            }
        }
    }

    //printf("Total accrued step 1 %d \n", degree);
    /* Codimension from NW white checkers*/

    // First iterate over white checkers across rows
    for(int i = 0; i < 2*n; i++) {
        if (wb_col[i] != -1) {
            // Iterate over previous rows' white checkers
            for (int j = 0; j <= i; j++) {
                if(wb_col[j] != -1 && wb_col[j] <= wb_col[i]) {
                    degree--;
                }
            }
        }
    }

    //printf("Total after step 2 %d \n", degree);

    /* Symmetric pairs condition */

    for(int i = 0; i < 2*n; i++) {
        if (wb_col[i] != -1) {
            // Iterate over previous rows' white checkers
            for (int j = 0; j < i; j++) {
                if(wb_col[j] != -1) {
                    int paired = 0;
                    int k = 0;
                    while (k <= j && paired != 1) {
                        if (bb_col[k] <= wb_col[j] && (bb_col[2*n - 1 - k] <= wb_col[i]) && (2*n-1-k <= i)) {
                            //printf("Symm subtraction w1 %d w2 %d b1 %d b2 %d \n", wb_col[i], wb_col[j], bb_col[k], bb_col[2*n - 1 - k]);
                            //printf(" k here is %d \n", k);
                            degree--;
                            paired = 1;
                        }
                        k++;
                    }
                }
            }
        }
    }

    return degree;

}