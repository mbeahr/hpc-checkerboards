#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <math.h>
#include <time.h>
#include <string.h>

#include "grass.h"

int main(int argc, char** argv) {
    
    if (argc < 4) { 
        printf("usage: ./grass test_num file_flag test_flag\n \n ");
        return(-1);
    }

    uint64_t test_num = strtoull(argv[1], NULL, 10);

    if (test_num >= bit_pow4((uint64_t) N)*factorial((uint64_t) N)) {
        printf("error: test_num %ld too large, max for N=%d is %ld \n", test_num, N, bit_pow4(N)*factorial(N));
        return(-1);
    }

    int file_flag = atoi(argv[2]);
    int test_flag = atoi(argv[3]);

    int* w_test = (int*) calloc(2*N, sizeof(int));

    FILE *out;
    if (file_flag) {
        out = fopen("grass.out", "w");
        if (out == NULL) {
            printf("error: cannot open grass.out");
            return(-1);
        }
    }

    uint64_t valid_found = 0;

    clock_t start, end;

    start = clock();
    for (uint64_t i = 0; i <= test_num; i++){
        num_to_board(i, w_test, N);

        int validNW = check_NW(b_test, w_test, N);
        int deg = calc_deg(b_test, w_test, N);
        
        if (validNW && (deg == VDEG)) {
            valid_found++;
            if (file_flag) {
                fprintf(out, "%" PRIu64 "\n", i);
            } 
        }
    }
    end = clock();

    if (test_flag) {
        printf("Testing against: \n");
        int empty[2*N];
        memset(empty, -1, sizeof(empty)); 
        print_comp_board(b_test, empty, N);
    }

    printf("Execution time for %" PRIu64 " is %f \n ", test_num, ((double)(end - start))/CLOCKS_PER_SEC);
    if (file_flag) {
        printf("Valid found out of %" PRIu64 " was %" PRIu64 " \n \n", test_num, valid_found);
    }

    if (file_flag) {
        fclose(out);
    }

    return 0;

}
