#include <stdbool.h>
#include <stdint.h>

#define N 4

#define VDEG 3

static int b_test[2*N] = {0,1,2,3,4,5,6,7};

/* HELPER METHODS */
int arg_in_arr(int val, int* arr, int len);

uint64_t factorial(uint64_t n);

uint64_t bit_pow4(uint64_t n);

/* BOARD METHODS */
void print_comp_board(int* bb_col, int* wb_col, int n);

void num_to_board(uint64_t state, int* c_col, int n);

/* CONDITION METHODS*/
bool check_NW(int* bb_col, int* wb_col, int n);

int calc_deg(int* bb_col, int* wb_col, int n);

