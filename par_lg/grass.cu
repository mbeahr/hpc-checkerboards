#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <cuda.h>
#include <malloc.h>
#include <math.h>
#include <time.h>

extern "C" {
    #include "grass.h"
}

int main(int argc, char** argv) {

    /* Initial setup */
    if(argc != 3) {
        printf("usage: ./grass num_test print_flag \n");
        return(-1);
    }
    
    /* Getting potentially very large number of tests to do from command line */
    uint64_t num_test = strtoull(argv[1], NULL, 10);
    int print_flag = atoi(argv[2]);


    uint64_t tot = bit_pow4(N)*factorial(N);

    if (num_test >= tot) {
        printf("Invalid number of tests, please use 1 < num_tests < %" PRIu64 " \n", tot);
        return(-1);
    }

    /* Running on infer again */
    cudaSetDevice(GPU_ID);

    uint64_t* c_boards;
    cudaMalloc(&c_boards, BATCH_SIZE*sizeof(uint64_t));
    cudaMemset(c_boards, 0, BATCH_SIZE*sizeof(uint64_t));

    FILE* out;
    uint64_t* boards = (uint64_t *) calloc(BATCH_SIZE, sizeof(uint64_t));
    uint64_t valid = 0;

    if (print_flag) {
        out = fopen("grass.out", "w");
        if (out == NULL) {
            printf("error: cannot open grass.out");
            return(-1);
        }
        /* See grass.h */
        if (INIT_BB) {
            fprintf(out, "0\n");
            valid ++;
        }
    }

    clock_t start, end;

    start = clock();
    uint64_t batches = (num_test + BATCH_SIZE - 1) / BATCH_SIZE; 
    for (uint64_t i = 0; i < batches; i++) {
        cudaCheckNW(c_boards, i*BATCH_SIZE);
        cudaCheckDeg(c_boards, DEGREE);   
        cudaMemcpy(boards, c_boards, BATCH_SIZE*sizeof(uint64_t), cudaMemcpyDeviceToHost);
        for (int i = 0; i < BATCH_SIZE; i++) {
            if(boards[i] != 0) {
                if (print_flag) {
                    fprintf(out, "%" PRIu64 "\n", boards[i]);
                    
                }
                valid++;
            }
        }
        cudaMemset(c_boards, 0, BATCH_SIZE*sizeof(uint64_t));
    }
    end = clock();

    if (print_flag) {
        fclose(out);
    }

    printf("Execution time for %" PRIu64" is %f \n ", num_test, ((double)(end - start))/CLOCKS_PER_SEC);
    printf("Valid boards %" PRIu64" \n", valid);

    free(boards);
    cudaFree(c_boards);

    return 0;

}
