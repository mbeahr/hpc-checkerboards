#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <cuda.h>

extern "C" {
    #include "grass.h"
}

/* Forms boards from state and checks that every white checker 
    has a black checker North of it and a black checker West of it */
__global__ void checkNWKernel(uint64_t *board_set, uint64_t batch_start) {
    
    uint64_t state = threadIdx.x + (blockDim.x)*(blockIdx.x) + batch_start;

    int wcol[2*N] = {0};
    int i, j;
    int j_placement[N] = {0};
    uint64_t fac = (bit_pow4(N))*factorial(N);

    /* Terminate threads that are out of bounds. */
    if (state >= fac) {
        return;
    } 

    int state_temp = state;
    for (i = N-1; i >= 0; i--) {
        fac = fac / (4*(i+1));
        j_placement[N-1-i] = state_temp / fac;
        state_temp = state_temp % fac;
    }

    int placed[2*N] = {0};
    int count;
    for (i = 0; i < N; i++) {
        count = j_placement[i];
        int sec = 0;

        /* In this case, we want to note to place the checker
          on the symm row and treat count as a count mod a full row */
        if (count >= 2*(N-i)) {
            sec = 1;
            count -= 2*(N-i);
        }

        j = 0;
        while (count >= 0 && j < 2*N) {
            if(count == 0 && placed[j] == 0) {
                count--;
                placed[j] = 1;
                placed[2*N-1-j] = 1;
                if (sec) {
                    wcol[2*N-1-i] = j; //(2*n -1) - j;
                    wcol[i] = -1;
                } else {
                    wcol[i] = j;
                    wcol[2*N-1-i] = -1;
                }
            } else if (placed[j] == 0) {
                count--;
            }
            j++;
        }
    }

    /* Board has been formed, can check NW first */
    // Checks for west
    i = 0;
    j = 0;
    while ((i < 2*N) && (j != -1)) {
        if((bcol[i] > wcol[i]) && (wcol[i] != -1)) {
            j = -1;
        }
        i++;
    }

    // Checks for north
    int checked = 0;
    int k = 0;
    while (checked < 2*N && checked >= 0) {
        k = 0;
        while (k < 2*N) {
            if (bcol[k] == checked) {
                k = 2*N;
            } else if(wcol[k] == checked) {  
                checked=-2;
            }
            k++;
        }
        checked++;
    } 
    
    /* Report board state if state passed NW check */
    if((state - batch_start < BATCH_SIZE) && (state < (bit_pow4(N))*factorial(N))) {
        if (checked >= 0 && j != -1) {
            board_set[state - batch_start] = state;
        } else {
            board_set[state - batch_start] = 0;
        } 
    }
}

/* Calculations to be done in batches based on 
    'reasonable' array size to be allocated on 
    DEVICE memory - board_set holds these values */
void cudaCheckNW(uint64_t * board_set, uint64_t batch_start) {

    uint64_t tot_boards = (bit_pow4(N))*(factorial(N));

    int B = p_B;
    int G; 
    
    /* When there's only a lil teeny bit to do at da end */
    if ((tot_boards - batch_start)/ BATCH_SIZE == 0) {
        G = ((tot_boards - batch_start) + B-1)/B;
    } 
    /* When there's more */
    else {
        G = (BATCH_SIZE + B-1)/B;
    }

    checkNWKernel <<< G, B >>> (board_set, batch_start);

}