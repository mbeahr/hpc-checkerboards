#ifndef GRASS_H
#define GRASS_H
#endif

#include <stdint.h>
#include <stdbool.h>
#include <cuda.h>

/* Running on infer again, so removed */
#define GPU_ID 1

/* Since basically everything is memory accesses, 
    I'd prefer to have  warp using its own register. */
#define p_B 64

/* DRAM for 1080ti is 11GB - so 2.75 mil ints at max 
    check first on 1 mil */
#define BATCH_SIZE 10000

/* WARNING: Using uint64_t at the moment, so maximum acceptable N 
    is 13. There are no checks within bit_pow(), factorial(), etc.
    that will warn the user if N is too high. */
#define N 7

#define DEGREE 3

/* Black board to check against, utilized by kernels in various files */
__device__ static int bcol[2*N] = {13,12,11,10,9,8,7,6,5,4,3,2,1,0};

/* defines whether we check the initial board, which is 
    the only one that the 0 state config satisfies. Less work than
    to use the board_methods functions in grass.cu initial step */
#define INIT_BB 0

/* HELPER METHODS */

__host__ __device__ uint64_t factorial(uint64_t n);

__host__ __device__ uint64_t bit_pow4(uint64_t n);

void print_comp_board(int* bb_col, int* wb_col, int n);

/* CONDITION METHODS*/

void cudaCheckNW(uint64_t * board_set, uint64_t batch_start);

void cudaCheckDeg(uint64_t *board_set, int deg_check);

void cudaCheckDegComp(uint64_t *board_set, int deg_check); 