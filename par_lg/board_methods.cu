#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <limits.h>
#include <cuda.h>

extern "C" {
    #include "grass.h"
}

__host__ __device__ uint64_t factorial(uint64_t n) {
    unsigned int res = n;
    for (uint64_t i = 2; i < n; i++) {
        res = res * i;
    }
    return res;
}

__host__ __device__ uint64_t bit_pow4(uint64_t n) {
    uint64_t res = 4;
    for (uint64_t i = 1; i < n; i++) {
        res = res * 4;
    }
    return res;
}

// Takes in a set of 2*n black checkers (x,y positions) and n white checkers (in a 2*n array)
// and prints the resulting board visually.
void print_comp_board(int* bb_col, int* wb_col, int n) {
    
    int i, j;
    for (i=0; i<(2*n); i++) {
        printf("\n");

        int bpos = bb_col[i];
        int wpos = wb_col[i];

        /* -1 used as marker for no wc on that row */
        if ((bpos < 0 || bpos >= (2*n)) || (wpos < -1 || (wpos >= (2*n) && wpos != -1))) {
            printf("Col %d has invalid positions, bc col %d, wc col %d \n \n", i, bpos, wpos);
            printf("n: %d \n ", n);
            return;
        }

        for (j=0; j<2*n; j++) {
            
            if (bpos == j) {
                if (wpos == j) {
                    printf(" B ");
                } else {
                    printf(" X ");
                }
            }
            else if(wpos == j) {
                printf(" O ");
            }
            else {
                printf(" . ");
            }
        }   
    }
    printf("\n \n");
}