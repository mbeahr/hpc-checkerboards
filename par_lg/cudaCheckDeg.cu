#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <cuda.h>

extern "C" {
    #include "grass.h"
}

/* This will be done after the initial test, meaning we need only 
    read values from the board_set. Note combining both checks
    in a single kernel would cause (even more) load imbalance. */
__global__ void checkDegKernel(uint64_t *board_set, int deg_check) {

    int ind = threadIdx.x + (blockDim.x)*(blockIdx.x);

    if (ind >= BATCH_SIZE) {
        return;
    }
    
    /* Now we get state from the board_set, since 0 entries will
        not be checked (they did not satisfy prev. NW check) */
    uint64_t state = board_set[ind];
    uint64_t fac = (bit_pow4(N))*factorial(N);

    /* Terminate threads that form invalid board configs or
        did not satisfy NW check */
    if (state >= fac || state == 0) {
        return;
    } 

    /* Same work as in checkNW kernel to calculate board from state */
    int wcol[2*N] = {0};
    int i, j;
    int j_placement[N] = {0};
    int state_temp = state;
    for (i = N-1; i >= 0; i--) {
        fac = fac / (4*(i+1));
        j_placement[N-1-i] = state_temp / fac;
        state_temp = state_temp % fac;
    }

    int placed[2*N] = {0};
    int count;
    for (i = 0; i < N; i++) {
        count = j_placement[i];
        int sec = 0;

        /* In this case, we want to note to place the checker
          on the symm row and treat count as a count mod a full row */
        if (count >= 2*(N-i)) {
            sec = 1;
            count -= 2*(N-i);
        }

        j = 0;
        while (count >= 0 && j < 2*N) {
            if(count == 0 && placed[j] == 0) {
                count--;
                placed[j] = 1;
                placed[2*N-1-j] = 1;
                if (sec) {
                    wcol[2*N-1-i] = j; //(2*n -1) - j;
                    wcol[i] = -1;
                } else {
                    wcol[i] = j;
                    wcol[2*N-1-i] = -1;
                }
            } else if (placed[j] == 0) {
                count--;
            }
            j++;
        }
    }
    /* BOARD FORMED, now check degree */

    int degree = 0;
    /* Degree accrued from black checkers*/
    // First iterate over white checkers across rows
    for(int i = 0; i < 2*N; i++) {
        if (wcol[i] != -1) {
            // Iterate over previous rows' black checkers
            for (int j = 0; j <= i; j++) {
                if(bcol[j] <= wcol[i]) {
                    degree++;
                }
            }
        }
    }

    /* Codimension from NW white checkers*/
    // First iterate over white checkers across rows
    for(int i = 0; i < 2*N; i++) {
        if (wcol[i] != -1) {
            // Iterate over previous rows' white checkers
            for (int j = 0; j <= i; j++) {
                if(wcol[j] != -1 && wcol[j] <= wcol[i]) {
                    degree--;
                }
            }
        }
    }
    
    /* Symmetric pairs condition */
    for(int i = 0; i < 2*N; i++) {
        if (wcol[i] != -1) {
            // Iterate over previous rows' white checkers
            for (int j = 0; j < i; j++) {
                if(wcol[j] != -1) {
                    int paired = 0;
                    int k = 0;
                    while (k <= j && paired != 1) {
                        if (bcol[k] <= wcol[j] && (bcol[2*N - 1 - k] <= wcol[i]) && (2*N-1-k <= i)) {
                            degree--;
                            paired = 1;
                        }
                        k++;
                    }
                }
            }
        }
    }

    /* We can leave the board state as is unless it 
       doesn't satisfy the check */
    if (degree != deg_check) {
        board_set[ind] = 0;
    }

}

void cudaCheckDeg(uint64_t *board_set, int deg_check) {

    int B = p_B;
    int G;

    /* Will only ever operate on the BATCH_SIZE array of 
        previously checked states */
    G = (BATCH_SIZE + B - 1)/B;

    checkDegKernel <<< G, B >>> (board_set, deg_check);

}