# HPC-Checkerboards

## Basics 

This repo is dedicated to performing parallel invariant calculations to compute geometric Littlewood-Richardson coefficients. The repo consists of seq_lg, including sequential methods for computation and verification of outputs, and par_lg, consisting of parallel implementations of these invariant checks. 

To get started, get a piece of paper, pick an $N<9$, and draw a $2N \times 2N$ checkerboard! Place $2N$ black checkers on the board such that:

- No two black checkers occupy the same row or column

- Any square which contains a black checker, when reflected across the horizontal and vertical midline, also has a black checker

- The NW quadrant of the board has no black checkers above its sub-diagonal

Not all such configurations are part of a valid 'checker game,' but if you want to be sure you are testing with a valid black checker configuration, two easy ones are given by 

- The configuration of $2N$ black checkers on the antidiagonal (the starting configuration)

- The configuration of $2N$ black checkers on the diagonal (the ending configuration)

From here, just express the configuration as an array of length $2N$ where indices are rows and values are columns (0-indexed) 

Next, you can do a few things: 

### Sequential LG

Using the tools provided in seq_lg, you can:

- Print a particular board configuration of black and white checkers

- Check whether it satisfies the northwest condition (every white checker has a northern black checker partner on the same column and a western black checker partner on the same row)

- Calculate the degree of the board

To do any of these, first set your N and your b_test black checker configuration in grass.h, then use make to compile print, grass, or degree

- './print' takes one argument - a state value (uint64_t) to convert to a white checker configuration that ranges from $0$ to $4^N * N!$. It then prints to the standard output:
    
    - The array of column indices of the white checker configuration resulting from that state, indexed by row.

    - The board resulting from that configuration of white checkers and the chosen configuration of black checkers in the header. '.' represents an empty space, 'X' a black checker, 'O' a white checker, and 'B' a square occupied by both a white and black checker.

    - Whether that board satisfies the northwest check

- './grass' takes three arguments - a test_num of states from $0$ to test_num to test against, a flag file_flag (either 0 or 1) and a test_flag (0 or 1). It should also be noted that one should set a VDEG in the header to use this properly. It then:

    - Tests all boards from $0$ to test_num to see whether they satisfy the northwest check and have the specified degree VDEG. It then prints the execution time for checking those boards

    - If file_flag is 1, it will print the resulting valid states to a file 'grass.out' and will output the number of valid boards found

    - If test_flag is 1, it will print a visual representation of the black board against which we are testing (which is the one set in the header)

- './degree' takes a single argument - a state value of the same format as described for './print' and prints the degree of the board with the specified black checker and white checker configurations

### Parallel LG

Using the tools provided in seq_lg, you can:

- Calculate which boards satisfy the northwest condition and have matching degree via computation on the GPU

As with Sequential LG, first set your N and your b_test black checker configuration in grass.h. If you wish to check against a particular degree, set a degree in the header as well, otherwise, comment out the line 'cudaCheckDeg(c_boards, DEGREE);' in grass.cu.

Then, you can compile using make and use './grass', which takes two arguments - a state value (uint64_t) and a print flag (0 or 1). 

- The state describes how many boards will be tested against (from 0 to state). If you don't know how many states there are, feel free to enter a state number and the program will let you know if you've gone too high.  

- The print flag decides whether the program will output the valid board states to 'grass.out' (1) or not (0). 

- This program will always print the execution time for calculating these things.

Lastly, you can adjust the BATCH_SIZE in the header, which decides how many boards the GPU works with at a time. Note that one shouldn't make this array larger than (or realistically even close to) the available DRAM for your specified device (The arrays are uint64_t, so 8 bytes per entry).

This code was intended to be run on the Pascal compute cluster at Virginia Tech with 1080ti GPUs, so change the architecture spec in the makefile if working on a different setup.

And that's it for now! Feel free to test various board configurations and states, but note that invalid black checker configurations may yield odd results (negative degrees, etc.)


## Authors and acknowledgment
Mason 'Three' Beahr

## License
For open source projects, say how it is licensed.

## Project status
Currently working on implementing the Rectangle Condition Check.
